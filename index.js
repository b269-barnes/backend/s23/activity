// Real World Application Of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/

let pokeMaster = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
};

console.log(pokeMaster)
console.log("Result of dot notation:")
console.log(pokeMaster.name)
console.log("Result of square bracket notation:")
console.log(pokeMaster.pokemon)
console.log('Result of talk method')
console.log(pokeMaster.talk())


function Pokemon(name, level, health, attack) {
    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * health;
    this.attack = attack;

    // Method
    this.tackle = function(opponent) {
        let remainingHealth = opponent.health - this.attack;
        console.log(this.name + ' tackled ' + opponent.name);
        console.log(opponent.name + "'s health is now reduced to " + remainingHealth);
        if (remainingHealth <= 0) {
            console.log(opponent.name + ' fainted.');
            opponent.level = 1; // 
            opponent.health = 0; // 
        } else {
            opponent.health = remainingHealth;
        }
    };
}


// Create new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon('Pikachu', 12, 100, 12);
let geodude = new Pokemon('Geodude', 8, 80, 8);
let mewtwo = new Pokemon('Mewtwo', 100, 200, 200  );
let rattata = new Pokemon('Rattata', 8, 80, 13 );




console.log (pikachu);
console.log (geodude);
console.log (mewtwo);


// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata)
console.log(rattata);

mewtwo.tackle(geodude)
console.log(geodude)
